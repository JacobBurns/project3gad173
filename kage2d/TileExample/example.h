#pragma once

#include "app.h"
#include "kage2dutil/physics.h"
#include "kage2dutil/atlas.h"	// Note: added so the atlas class can be put in the Example class.
#include <TileMap.h>

class Example : public App
{
public:
	Example();
	virtual ~Example();
	virtual bool start();
	virtual void update(float deltaT);
	virtual void render();
	virtual void cleanup();
	static Example &inst();

	TileMap m_tileMap;

	enum GameState
	{
		e_editor,
		e_game
	};
	void updateGame(float deltaT);
	void updateEditor(float deltaT);
	void setState(GameState newState);
	void enterStateGame();
	void enterStateEditor();

	int m_currentLayer;
	int m_currentTile;

	GameState m_currentState = GameState::e_editor; // technically not the right way to write the code.

};
