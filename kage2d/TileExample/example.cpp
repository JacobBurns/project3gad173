#include "example.h"
#include "TileMap.h"
#include "Rabbit.h"
#include "FrostGaint.h"
#include <Windows.h>

Example::Example(): App()
{
}

Example::~Example()
{
}

Example &Example::inst()
{
	static Example s_instance;
	return s_instance;
}

void Example::updateEditor(float deltaT)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::G))
	{
		setState(GameState::e_game);
	}
	//kage::World::setView(m_window, kf::Vector2(15, 32), 0.5);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) && m_window.hasFocus())
	{
		m_running = false;
	}
	kf::Vector2i mousePos = m_window.mapPixelToCoords(sf::Mouse::getPosition(m_window));
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !ImGui::GetIO().WantCaptureMouse && m_window.hasFocus())
	{
		int x = mousePos.x / m_tileMap.GetTileWidth();
		int y = mousePos.y / m_tileMap.GetTileHeight();
		m_tileMap.SetTile(x, y, m_currentLayer, m_currentTile);
	}

	ImGui::Begin("Kage2D");
	if (ImGui::Button("Load"))
	{
		char filename[MAX_PATH];
		OPENFILENAME ofn;
		ZeroMemory(&filename, sizeof(filename));
		ZeroMemory(&ofn, sizeof(ofn));
		ofn.lStructSize = sizeof(ofn);
		ofn.hwndOwner = m_window.getSystemHandle();
		ofn.lpstrFilter = "CSV Files\0*.csv\0Any File\0*.*\0";
		ofn.lpstrFile = filename;
		ofn.nMaxFile = MAX_PATH;
		ofn.lpstrTitle = "Select destination for export";
		ofn.Flags = OFN_NOCHANGEDIR;

		if (GetOpenFileNameA(&ofn)) 
		{
			m_tileMap.Load(filename);
		}

	}
	if (ImGui::Button("Save"))
	{
		char filename[MAX_PATH];
		OPENFILENAME ofn;
		ZeroMemory(&filename, sizeof(filename));
		ZeroMemory(&ofn, sizeof(ofn));
		ofn.lStructSize = sizeof(ofn);
		ofn.hwndOwner = m_window.getSystemHandle();
		ofn.lpstrFilter = "CSV Files\0*.csv\0Any File\0*.*\0";
		ofn.lpstrFile = filename;
		ofn.nMaxFile = MAX_PATH;
		ofn.lpstrTitle = "Select destination for export";
		ofn.Flags = OFN_NOCHANGEDIR;

		if (GetSaveFileNameA(&ofn))
		{
			m_tileMap.Save(filename);
		}
	}
	if (ImGui::Button("Exit"))
	{
		m_running = false;
	}
	if (ImGui::Button("Background"))
	{
		m_currentLayer = 0;
	}
	ImGui::SameLine();
	if (ImGui::Button("Foreground"))
	{
		m_currentLayer = 1;
	}
	if (m_currentLayer == 0)
	{
		ImGui::Text("Layer: Background");
	}
	else
	{
		ImGui::Text("Layer: Foreground");
	}

	for (int i = 0; i < m_tileMap.GetTileDefinitions().size(); ++i)
	{
		TileDefinition td = m_tileMap.GetTileDefinitions()[i];
		m_tileMap.GetAtlas().selectTile(td.tileX, td.tileY);
		ImGui::PushID(i);
		if (ImGui::ImageButton(m_tileMap.GetAtlas()))
		{
			m_currentTile = i;
		}
		ImGui::PopID();
	}
	ImGui::End();

}

void Example::updateGame(float deltaT)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::E))
	{
		setState(GameState::e_editor);
	}
}

void Example::setState(GameState newState)
{
	m_currentState = newState;
	if (m_currentState == GameState::e_editor)
	{
		enterStateEditor();
	}
	else
	{
		enterStateGame();
	}
}

void Example::enterStateGame()
{
	sf::Sound* sound = kage::SoundManager::playSound("data/Soundbg.ogg");
	sound->setLoop(true);
	sound->setVolume(5);
	for (int y = 0; y < m_tileMap.m_mapHeight; ++y)
	{
		for (int x = 0; x < m_tileMap.m_mapWidth; ++x)
		{
			int tile = m_tileMap.GetTile(x, y, TileMap::layers::e_foreground);
			TileDefinition def = m_tileMap.GetTileDefinitions()[tile];
			if (tile == TileMap::TileTypes::e_spawnPlayer)
			{
				Rabbit* player = kage::World::build<Rabbit>();
				player->position(x + 0.5, y + 0.5);
			}
			else if (tile == TileMap::TileTypes::e_spawnMonster)
			{
				FrostGaint* enemy = kage::World::build<FrostGaint>();
				enemy->position(x + 0.5, y + 0.5);
			}
			else if (tile == TileMap::TileTypes::e_spawnMonster)
			{

			}
			else if (def.blocking)
			{
				kage::Physics::BoxBuilder().pos(kf::Vector2(x + 0.5,y + 0.5)).userData(tile).size(1, 1).build(kage::Physics::getDefaultStatic());
			}
		}
	}
}

void Example::enterStateEditor()
{
	kage::World::setView(m_window);
	kage::World::clear();
	kage::Physics::clearDefaultStaticFixtures();
}

bool Example::start()
{	
	kage::World::scale(32);
	m_tileMap.SetMapSize(32, 32);
	m_currentLayer = 1;
	m_currentTile = 0;

	return true;
	
}

void Example::update(float deltaT)
{
	kage::SoundManager::update();

	switch (m_currentState)
	{
	case GameState::e_editor:
			updateEditor(deltaT);
			break;
	case GameState::e_game:
				updateGame(deltaT);
				break;
	}
}

void Example::render()
{
	m_tileMap.Render(m_window);

	kage::Physics::debugDraw(&m_window, 64);
	kage::drawBox(m_window, kf::Vector2(0, 0), kf::Vector2(m_tileMap.GetMapWidth() * m_tileMap.GetTileWidth(), m_tileMap.GetMapHeight() * m_tileMap.GetTileHeight()), sf::Color::Green);
}

void Example::cleanup()
{

}

