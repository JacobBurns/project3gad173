#pragma once
#include "kage2dutil/gameobject.h"
#include "kage2dutil/anim.h"

class Rabbit : public kage::GameObject
{
public:
	Rabbit();
	~Rabbit();

	//void render();
	void update(float deltaT);
	void onCollision(GameObject *obj);
	void onCollision(b2Fixture *fix);


	int m_health = 10;
	int m_score = 0;
	kage::Anim m_anim;

};
