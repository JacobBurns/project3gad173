#pragma once
#include <vector>
#include "kage2dutil/physics.h"
#include "kage2dutil/atlas.h"

using namespace std;

struct TileDefinition
{
	bool blocking;
	bool visibleInGame;
	int tileX;
	int tileY;

};

class TileMap
{

public:

	int m_mapWidth;
	int m_mapHeight;
	int m_tileWidth;
	int m_tileHeight;
	vector<int> m_tiles[2];
	vector<TileDefinition> m_tileDefinition;
	kage::Atlas m_atlas;

	enum layers
	{
		e_background,
		e_foreground
	};

	enum TileTypes
	{
		e_empty = 0,
		e_lava,
		e_brick,
		e_redBlock,
		e_spawnMonster,
		e_spawnPlayer
	};

	TileMap();
	virtual void SetMapSize(int width, int height);
	virtual void SetTileSize(int width, int height);
	int GetMapWidth();
	int GetMapHeight();
	int GetTileWidth();
	int GetTileHeight();
	vector<TileDefinition>& GetTileDefinitions();
	void Render(sf::RenderTarget& target);
	void Clear(int tile, int layer);
	void Load(string filename);
	void Save(string filename);
	int GetTile(int x, int y, int layer);
	void SetTile(int x, int y, int layer, int tile);
	kage::Atlas& GetAtlas();

};
