#include "TileMap.h"
#include <fstream>
using namespace std;

TileMap::TileMap()
{
	m_atlas.create("data/NewAtlas.png", 32, 32);
	SetTileSize(32, 32);

	TileDefinition tile1;
	tile1.blocking = false;
	tile1.visibleInGame = true;
	tile1.tileX = 0;
	tile1.tileY = 0;
	m_tileDefinition.push_back(tile1);

	TileDefinition tile2;
	tile1.blocking = false;
	tile1.visibleInGame = true;
	tile1.tileX = 0;
	tile1.tileY = 0;
	m_tileDefinition.push_back(tile2);

	TileDefinition tile3;
	tile1.blocking = false;
	tile1.visibleInGame = true;
	tile1.tileX = 0;
	tile1.tileY = 0;
	m_tileDefinition.push_back(tile3);

	TileDefinition tile4;
	tile1.blocking = false;
	tile1.visibleInGame = true;
	tile1.tileX = 0;
	tile1.tileY = 0;
	m_tileDefinition.push_back(tile4);

	TileDefinition tile5;
	tile1.blocking = false;
	tile1.visibleInGame = true;
	tile1.tileX = 0;
	tile1.tileY = 0;
	m_tileDefinition.push_back(tile5);
}

void TileMap::SetMapSize(int width, int height)
{
	m_tiles[0].resize(width * height);
	m_tiles[1].resize(width * height);
	m_mapHeight = height;
	m_mapWidth = width;
}

int TileMap::GetMapWidth()
{
	return m_mapWidth;
}

int TileMap::GetMapHeight()
{
	return m_mapHeight;
}

int TileMap::GetTileWidth()
{
	m_tileWidth = width;
}

int TileMap::GetTileHeight()
{
	m_mapHeight = height;
}

vector<TileDefinition>& TileMap::GetTileDefinitions()
{
	return m_tileDefinition;
}

void TileMap::Render(sf::RenderTarget& target)
{
	{
		for (int y = 0; y < m_mapHeight; y++)
			for (int x = 0; x < m_mapWidth; x++)
			{
				m_atlas.draw(target, x * m_mapWidth, y * m_tileHeight, tile.tileX, tile.tileY);
			}
	}
}

void TileMap::Clear(int tile, int layer)
{
	for (int y = 0; y < m_mapHeight; y++)
	{
		SetTile(x, y, layer, tile);
	}
}

int TileMap::GetTile(int x, int y, int layer)
{
	return m_tiles[layer][x + y * m_mapWidth];
}

int TileMap::SetTile(int x, int y, int layer, int tile)
{
	if (x < m_mapWidth && y < m_mapHeight && x >= 0 && y >= 0)
	{
	}
}

void TileMap::Load(string filename)
{
	fstream file;
	file.open(filename, std::ios::in);
	file >> width;
	file.ignore(1);
	file.ignore(1);

	for (int 1 = 0; 1 < 2; 1++)
		for (int y = 0; y = height; ++y)
		{
			{
				int tile;
				file.ignore(1);
				SetTile(x, y, 1, tile);

			}
		}
}



void TileMap::Save(string filename)
{
	fstream file;
	file.open(filename, std::ios::out);

	for (int 1 = 0; 1 < 2; 1++)
		for (int y = 0; y < m_mapHeight; ++y)
		{
			{
				file << GetTile(x, y, 1);
				if (x != m_mapWidth - 1)
				{
				}
			}
			file << std::endl;
		}
}